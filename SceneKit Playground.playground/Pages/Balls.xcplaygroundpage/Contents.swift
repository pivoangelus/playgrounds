import Cocoa
import Foundation
import SceneKit
import XCPlayground
import SpriteKit

let sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: 800, height: 600))
XCPlaygroundPage.currentPage.liveView = sceneView

var scene = SCNScene()
sceneView.scene = scene
sceneView.backgroundColor = SKColor.whiteColor()
//sceneView.debugOptions = .ShowWireframe
sceneView.autoenablesDefaultLighting = true

var cameraNode = SCNNode()
cameraNode.camera = SCNCamera()
cameraNode.position = SCNVector3(x: 0, y: 0, z: 35)
scene.rootNode.addChildNode(cameraNode)


let purple = SCNSphere(radius: 1.5)
purple.firstMaterial?.diffuse.contents = NSColor.purpleColor()
let purpleNode = SCNNode(geometry: purple)
purpleNode.position = SCNVector3(x: 0.0, y: 5.0, z: 0.0)
scene.rootNode.addChildNode(purpleNode)

let blue = SCNSphere(radius: 1.5)
blue.firstMaterial?.diffuse.contents = NSColor.blueColor()
let blueNode = SCNNode(geometry: blue)
blueNode.position = SCNVector3(x: 5.0, y: 5.0, z: 5.0)
scene.rootNode.addChildNode(blueNode)

let green = SCNSphere(radius: 1.5)
green.firstMaterial?.diffuse.contents = NSColor.greenColor()
let greenNode = SCNNode(geometry: green)
greenNode.position = SCNVector3(x: 0.0, y: 0.0, z: 0.0)
scene.rootNode.addChildNode(greenNode)

let orange = SCNSphere(radius: 1.5)
orange.firstMaterial?.diffuse.contents = NSColor.orangeColor()
let orangeNode = SCNNode(geometry: orange)
orangeNode.position = SCNVector3(x: -5.0, y: 5.0, z: 5.0)
scene.rootNode.addChildNode(orangeNode)

let cylinder = SCNCylinder(radius: 0.5, height: 3)
cylinder.firstMaterial?.diffuse.contents = NSColor.brownColor()
let cylinderNode = SCNNode(geometry: cylinder)
cylinderNode.position = SCNVector3(x: -10.0, y: 5.0, z: 10.0)
scene.rootNode.addChildNode(cylinderNode)