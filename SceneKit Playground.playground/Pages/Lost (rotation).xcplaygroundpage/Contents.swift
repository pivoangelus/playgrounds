import Cocoa
import Foundation
import SceneKit
import XCPlayground
import SpriteKit

let sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: 800, height: 600))
XCPlaygroundPage.currentPage.liveView = sceneView

var scene = SCNScene()
sceneView.scene = scene
sceneView.backgroundColor = SKColor.blackColor()
sceneView.debugOptions = .ShowWireframe
sceneView.autoenablesDefaultLighting = true

var cameraNode = SCNNode()
cameraNode.camera = SCNCamera()
cameraNode.position = SCNVector3(x: 0, y: 0, z: 3)
scene.rootNode.addChildNode(cameraNode)

let text = NSAttributedString(string: "Lost", attributes: [NSFontAttributeName: NSFont(name: "Helvetica", size: 1.0)!])

let textGeometry = SCNText(string: text, extrusionDepth: 0.3)
let textNode = SCNNode(geometry: textGeometry)
textNode.rotation = SCNVector4(x: 1.0, y: 1.0, z: 0.0, w: 0.0)
scene.rootNode.addChildNode(textNode)


var spin = CABasicAnimation(keyPath: "rotation.w")
spin.toValue = 2.0 * M_PI
spin.duration = 8.0
spin.repeatCount = HUGE // for  infinity
//textNode.addAnimation(spin, forKey: "spin lost around")

