enum ChangoSpellError: ErrorType {
    case HatMissingOrNotMagical
    case NoFamiliar
    case FamiliarAlreadyAToad
    case SpellFailed(reason: String)
    case SpellNotKnownToWitch
}

enum MagicWords: String {
    case Abracadabra = "abracadabra"
    case Alakazam = "alakazam"
    case HocusPocus = "hocus pocus"
    case PrestoChango = "presto chango"
}

protocol MagicalTutorialObject {
    var avatar: String { get }
}

protocol Familiar: MagicalTutorialObject {
    var noise: String { get }
    var name: String? { get set }
    init()
    init(name: String?)
}

extension Familiar {
    init(name: String?) {
        self.init()
        self.name = name
    }
    func speak() {
        print(avatar, "* \(noise)s *")
    }
}

protocol MagicalBeing: MagicalTutorialObject {
    var name: String? { get set }
    var familiar: Familiar? { get set }
    var spells: [Spell] { get set }
    func turnFamiliarIntoToad() throws -> Toad
}

struct Spell: MagicalTutorialObject {
    var magicWords: MagicWords = .Abracadabra
    var avatar = "💫"
    
    init?(words: String) {
        guard let incantation = MagicWords(rawValue: words) else {
            return nil
        }
        self.magicWords = incantation
    }
    
    init?(magicWords: MagicWords) {
        self.magicWords = magicWords
    }
}

struct Cat: Familiar {
    var name: String?
    var noise = "purr"
    var avatar = "🐱"
}

struct Bat: Familiar {
    var name: String?
    var noise = "screech"
    var avatar = "[bat]"
    func speak() {
        print(avatar, "* \(noise)es *")
    }
}

struct Toad: Familiar {
    var name: String?
    var noise = "croak"
    var avatar = "🐸"
}

struct Hat {
    enum HatSize {
        case Small
        case Medium
        case Large
    }
    
    enum HatColor {
        case Black
    }
    
    var color: HatColor = .Black
    var size: HatSize = .Medium
    var isMagical = true
}

struct Witch: MagicalBeing {
    var avatar = "👩🏻"
    var name: String?
    var familiar: Familiar?
    var spells: [Spell] = []
    var hat: Hat?
    
    init(name: String?, familiar: Familiar?) {
        self.name = name
        self.familiar = familiar
        if let s = Spell(magicWords: .PrestoChango) {
            self.spells = [s]
        }
    }
    
    init(name: String?, familiar: Familiar?, hat: Hat?) {
        self.init(name: name, familiar: familiar)
        self.hat = hat
    }
    
    func speak() {
        defer {
            print("*crackles*")
        }
        
        defer {
            print("*screeches*")
        }
        
        print("Hello my pretties.")
    }
    
    func turnFamiliarIntoToad() throws -> Toad {
        guard let hat = hat where hat.isMagical else {
            throw ChangoSpellError.HatMissingOrNotMagical
        }
        
        guard let familiar = familiar else {
            throw ChangoSpellError.NoFamiliar
        }
        
        if familiar is Toad {
            throw ChangoSpellError.FamiliarAlreadyAToad
        }
        
        guard hasSpellOfType(.PrestoChango) else {
            throw ChangoSpellError.SpellNotKnownToWitch
        }
        
        guard let name = familiar.name else {
            let reason = "Familiar doesn't have a name."
            throw ChangoSpellError.SpellFailed(reason: reason)
        }
        
        return Toad(name: name)
    }
    
    func hasSpellOfType(type: MagicWords) -> Bool {
        return spells.contains { $0.magicWords == type }
    }
}

func handleSpellError(error: ChangoSpellError) {
    let prefix = "Spell Failed."
    switch error {
        case .HatMissingOrNotMagical:
            print("\(prefix) Did you forget your hat, or does it need its batteries charged?")
        case .FamiliarAlreadyAToad:
            print("\(prefix) Why are you trying to change a Toad into a Toad?")
        default:
            print(prefix)
    }
}

func exampleOne() {
    print("")
    let salem = Cat(name: "Salem Saberhagen")
    salem.speak()
    
    let witchOne = Witch(name: "Sabrina", familiar: salem)
    do {
        try witchOne.turnFamiliarIntoToad()
    }
    catch let error as ChangoSpellError {
        handleSpellError(error)
    }
    catch {
        print("Something went wrong, are you feeling okay?")
    }
}

func exampleTwo() {
    print("")
    let witchTwo = Witch(name: "Hermione", familiar: nil, hat: nil)
    witchTwo.speak()
}

exampleOne()
exampleTwo()