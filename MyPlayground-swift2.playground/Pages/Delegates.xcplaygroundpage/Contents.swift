import UIKit

// delegates

@objc protocol Speaker {
    func Speak ()
    optional func TellJoke()
}

protocol DateSimulatorDelegate {
    func dateSimulatorDidStart(sim: DateSimulator, a: Speaker, b: Speaker)
    func dateSimulatorDidEnd(sim: DateSimulator, a: Speaker, b: Speaker)
}

class LoggingDateSimulator: DateSimulatorDelegate {
    func dateSimulatorDidStart(sim: DateSimulator, a: Speaker, b: Speaker) {
        print("Date started!")
    }
    func dateSimulatorDidEnd(sim: DateSimulator, a: Speaker, b: Speaker) {
        print("Date ended!")
    }
}

// these conform to the protocol by implementing the func
class Vicki: Speaker {
    @objc func Speak() {
        print("Hello, I am Vicki")
    }
    @objc func TellJoke() {
        print("Q: What did Sushi A say to Sushi B?")
    }
}

class Ray: Speaker {
    @objc func Speak() {
        print("Hello, I am Ray")
    }
    @objc func TellJoke() {
        print("Q: Whats the object-oriented way to become wealthy?")
    }
    func WriteTutorial() {
        print("I'm on it")
    }
}

class DateSimulator {
    var delegate: DateSimulatorDelegate?
    let a: Speaker
    let b: Speaker
    
    init(a: Speaker, b: Speaker) {
        self.a = a
        self.b = b
    }
    
    func simulate() {
        delegate?.dateSimulatorDidStart(self, a: a, b: b)
        print("off to dinner..")
        a.Speak()
        b.Speak()
        print("walking back home..")
        a.TellJoke?()
        b.TellJoke?()
        delegate?.dateSimulatorDidEnd(self, a: a, b: b)
    }
}

let sim = DateSimulator(a: Vicki(), b: Ray())
sim.delegate = LoggingDateSimulator()
sim.simulate()


