import UIKit

// custom subscripting

enum PlayerColor: String {
    case None = "-"
    case Red = "r"
    case Black = "b"
    
    var description: String {
        get  {
            switch self {
            case .None:
                return "None"
            case .Red:
                return "Red"
            case .Black:
                return "Black"
            }
        }
    }
}

class GameBoard {
    var board: [[PlayerColor]] = []
    
    func empty() -> PlayerColor {
        return .None
    }
    
    func red() -> PlayerColor {
        return .Red
    }
    
    func black() -> PlayerColor {
        return .Black
    }
    
    init() {
        // "standard" board
        board = [
            [empty(),   red(),      empty(),    red(),      empty(),    red(),      empty(),    red()],
            [red(),     empty(),    red(),      empty(),    red(),      empty(),    red(),      empty()],
            [empty(),   red(),      empty(),    red(),      empty(),    red(),      empty(),    red()],
            [empty(),   empty(),    empty(),    empty(),    empty(),    empty(),    empty(),    empty()],
            [empty(),   empty(),    empty(),    empty(),    empty(),    empty(),    empty(),    empty()],
            [black(),   empty(),    black(),    empty(),    black(),    empty(),    black(),    empty()],
            [empty(),   black(),    empty(),    black(),    empty(),    black(),    empty(),    black()],
            [black(),   empty(),    black(),    empty(),    black(),    empty(),    black(),    empty()]
        ]
    }
    
    func displayBoard() {
        var outputString = ""
        for column in board {
            for square in column {
                outputString += square.rawValue
            }
            outputString += "\n"
        }
        print(outputString)
    }
    
    func movePieceFrom(from: (Int,Int), to: (Int,Int)) {
        //let pieceToMove = board[from.1][from.0]
        //board[from.1][from.0] = empty()
        //board[to.1][to.0] = pieceToMove
        
        // ~= checks to see if the number on the right is in the range of numbers on the left
        if !(0...7 ~= from.0 && 0...7 ~= from.1 && 0...7 ~= to.0 && 0...7 ~= to.1) {
            error("Range error")
            return
        }
        
        if playerAtLocation(from) == .None {
            error("No Piece to Move")
            return
        }
        
        if playerAtLocation(to) != .None {
            error("Move onto occupied square")
            return
        }
        
        let yDifference = to.1 - from.1
        // red will always move in a positive direction, black in a negative direction (based on our board)
                                              // if red                         if black
        if (playerAtLocation(from) == .Red) ? yDifference != abs(yDifference) : yDifference == abs(yDifference) {
            error("Move in wrong direction")
            return
        }
        
        if abs(to.0 - from.0) != 1 || abs(to.1 - from.1) != 1 {
            if abs(to.0 - from.0) != 2 || abs(to.1 - from.1) != 2 {
                error("Not a diagonal move")
            }
            let coorsOfJumpedPiece = ((to.0 + from.0) / 2 as Int, (to.1 + from.1) / 2 as Int)
            let pieceToBeJumped: PlayerColor = self[coorsOfJumpedPiece]
            if [pieceToBeJumped, playerAtLocation(from)].contains(.None) {
                error("Illegal jump")
                return
            }
            setPlayer(.None, atLocation: coorsOfJumpedPiece)
        }
    }
    
    func playerAtLocation(coordinates: (Int,Int)) -> PlayerColor {
        return board[coordinates.1][coordinates.0]
    }
    
    func setPlayer(player: PlayerColor, atLocation coordinates: (Int,Int)) {
        board[coordinates.1][coordinates.0] = player
    }
    
    func error(errorType: String) {
        print("Invalid move: \(errorType)")
    }
    
    subscript(coordinates: (Int, Int)) -> PlayerColor {
        get {
            return playerAtLocation(coordinates)
        }
        set {
            setPlayer(newValue, atLocation: coordinates)
        }
    }
    
    subscript(move coordinates: (Int, Int)) -> (Int, Int) {
        get {
            assert(false, "Using the getter of this subscript is not supported.")
        }
        set {
            movePieceFrom(coordinates, to: newValue)
        }
    }
}

let board = GameBoard()
//board.displayBoard()
//board.movePieceFrom((2,5), to: (3,4))
board[move: (2,5)] = (3,4)
//board.displayBoard()

let player = board[(3,4)]
print(player)
board[(3,4)] = .Red
//board.displayBoard()

board[move: (1,6)] = (2,5)
board[move: (1,6)] = (1,5)
board[move: (7,2)] = (8,1)

