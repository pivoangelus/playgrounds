import UIKit
import SpriteKit
import XCPlayground

var blowingRight = true
var windForce = CGVector(dx: 50, dy: 0)

let sceneView = SKView(frame: CGRect(x: 0, y: 0, width: 480, height: 320))
let scene = SKScene(size: CGSize(width: 480, height: 320))
sceneView.showsPhysics = true
sceneView.showsFPS = true
sceneView.presentScene(scene)
XCPlaygroundPage.currentPage.liveView = sceneView

let square = SKSpriteNode(imageNamed: "square")
square.name = "shape"
square.position = CGPoint(x: scene.size.width * 0.25, y: scene.size.height * 0.50)

let circle = SKSpriteNode(imageNamed: "circle")
circle.name = "shape"
circle.position = CGPoint(x: scene.size.width * 0.50, y: scene.size.height * 0.50)

let triangle = SKSpriteNode(imageNamed: "triangle")
triangle.name = "shape"
triangle.position = CGPoint(x: scene.size.width * 0.75, y: scene.size.height * 0.50)

let l = SKSpriteNode(imageNamed: "L")
l.name = "shape"
l.position = CGPoint(x: scene.size.width * 0.5, y: scene.size.height * 0.75)

scene.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
scene.physicsBody = SKPhysicsBody(edgeLoopFromRect: scene.frame)
circle.physicsBody = SKPhysicsBody(circleOfRadius: circle.size.width / 2)
square.physicsBody = SKPhysicsBody(rectangleOfSize: square.size)
triangle.physicsBody = SKPhysicsBody(texture: triangle.texture!, size: triangle.size)
l.physicsBody = SKPhysicsBody(texture: l.texture!, size: l.size)

scene.addChild(square)
scene.addChild(circle)
scene.addChild(triangle)
scene.addChild(l)

func spawnSand() {
    let sand = SKSpriteNode(imageNamed: "sand")
    sand.name = "sand"
    sand.position = CGPoint(
        x: random(min: 0.0, max: scene.size.width),
        y: scene.size.height - sand.size.height
    )
    sand.physicsBody = SKPhysicsBody(circleOfRadius: sand.size.height / 2)
    sand.physicsBody!.restitution = 1.0 // bouncy
    sand.physicsBody!.density = 20
    scene.addChild(sand)
}

delay(seconds: 2.0, completion: {
    scene.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)
    scene.runAction(
        SKAction.repeatAction(
            SKAction.sequence([
                SKAction.runBlock(spawnSand),
                SKAction.waitForDuration(0.1)
                ]), count: 100)
        )
    }
)

    NSTimer.scheduledTimerWithTimeInterval(0.05, target: scene, selector: "windWithTimer:", userInfo: nil, repeats: true)
    NSTimer.scheduledTimerWithTimeInterval(3.0, target: scene, selector: "switchWindDirection:", userInfo: nil, repeats: true)

extension SKScene {
    func windWithTimer(timer: NSTimer) {
        let nodes = scene?.children
        for node in nodes! {
            node.physicsBody?.applyImpulse(windForce)
        }
    }
    
    func switchWindDirection(timer: NSTimer) {
        blowingRight = !blowingRight
        windForce = CGVector(dx: blowingRight ? 50 : -50, dy: 0)
    }
}