import Foundation

func random_uniform(value: Int) -> Int {
    return Int(arc4random_uniform(UInt32(value)))
}

typealias JSONObject = [String: AnyObject]
let file = Bundle.main.path(forResource: "tutorials", ofType: "json")
let url = NSURL(fileURLWithPath: file!)
let data = NSData(contentsOf: url as URL)
let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [JSONObject]
//print (json)

enum Day: Int {
    case Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}

class Tutorial {
    let title: String
    var day: Day?
    
    init(title: String, day: Day? = nil) {
        self.title = title
        self.day = day
    }
}

extension Tutorial: CustomStringConvertible {
    var description: String {
        var scheduled = ", not scheduled"
        if let newDay = day {
            scheduled = ", scheduled on \(newDay)"
        }
        return title + scheduled
    }
}

var tutorials: [Tutorial] = []

for dictionary in json {
    var currentTitle = ""
    var currentDay: Day? = nil
    
    for (key, value) in dictionary {
        switch (key, value) {
        case ("title", is String):
            currentTitle = value as! String
        case ("day", let dayString as String):
            if let dayInt = Int(dayString) {
                currentDay = Day(rawValue: dayInt - 1)
            }
        default:
            break
        }
    }
    let currentTutorial = Tutorial(title: currentTitle, day: currentDay)
    tutorials.append(currentTutorial)
}

//print(tutorials)

tutorials.forEach {$0.day = nil}
let days = (0...6).map { Day(rawValue: $0)! }
let randomDays = days.sorted { _ in random_uniform(value: 2) == 0 }
(0...6).forEach { tutorials[$0].day = randomDays[$0] }

//print(tutorials)

tutorials.sort {
    switch ($0.day, $1.day) {
    case (nil, nil):
        return $0.title.compare($1.title, options: .caseInsensitive) == .orderedAscending
    case (let firstDay?, let secondDay?):
        return firstDay.rawValue < secondDay.rawValue
    case (nil, let secondDay?):
        return true
    case (let firstDay?, nil):
        return false
    }
}

//print (tutorials)

extension Day {
    var name: String {
        switch self {
        case .Monday:
            return "Monday"
        case .Tuesday:
            return "Tuesday"
        case .Wednesday:
            return "Wednesday"
        case .Thursday:
            return "Thursday"
        case .Friday:
            return "Friday"
        case .Saturday:
            return "Saturday"
        case .Sunday:
            return "Sunday"
        }
    }
}

func ~=(lhs: Int, rhs: Day) -> Bool {
    return lhs == rhs.rawValue + 1
}

extension Tutorial {
    var order: String {
        guard let day = day else {
            return "not scheduled"
        }
        switch day {
        case 1:
            return "first"
        case 2:
            return "second"
        case 3:
            return "third"
        case 4:
            return "fourth"
        case 5:
            return "fifth"
        case 6:
            return "sixth"
        case 7:
            return "seventh"
        default:
            fatalError("invalid day value")
        }
    }
}

for (index, tutorial) in tutorials.enumerated() {
    guard let day = tutorial.day else {
        print("\(index + 1). \(tutorial.title) is not scheduled this week.")
        continue
    }
    print("\(index + 1). \(tutorial.title) is scheduled on \(day.name). It's the \(tutorial.order) tutorial of the week.")
}
